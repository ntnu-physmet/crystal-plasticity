# Crystal plasticity

![RVE](RVE.PNG)

## Contributors
Tomas Manik (researcher), Bjørn Holmedal (Professor), Hassan Moradi (PhD candidate) in the Physical Metallurgy Group at the Department of Materials Science and Engineering, Norwegian University of Technology, Trondheim, Norway. 

https://www.ntnu.edu/ima/research/physical-metallurgy#/view/about

## About
A robust return-mapping algorithm for crystal-plasticity using arbitrary slip systems and regularized yield surface is implemented as a user-material subroutine (UMAT) in the finite element software Abaqus/Standard [1]. Numerical stability is ensured by applying a line search algorithm for each newton iteration, further improved by an initial guess for the stress solution. 

The main programs placed in directories all need the umatCP.f file.

## Example
The Abaqus input directory contains input files for generating a FE model of a representative volume element (RVE) for a polycrystalline material subjected to uniaxial tension, as discussed in [1]. Using the provided UMAT, you can run it as

```
abaqus job=CPFEM_size20_main.inp user=umatCP.for
```

[1] T. Mánik, H.M. Asadkandi, B. Holmedal. A robust algorithm for rate-independent crystal plasticity, Computer Methods in Applied Mechanics and Engineering, Volume 393. DOI https://doi.org/10.1016/j.cma.2022.114831

## Contact
Tomas Manik - tomas.manik@ntnu.no

Bjørn Holmedal - bjorn.holmedal@ntnu.no 
