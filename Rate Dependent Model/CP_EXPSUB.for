c======================================================================
c                                                                     *
c Hypotheses:                                                         *
c + Face centred cubic (FCC) crystals                                 *
c + Finite deformation (large strains)                                *
c + Constant velocity gradient                                        *
c + Isochoric plastic deformation                                     *
c + Isotropic thermal expansion                                       *
c                                                                     *
c======================================================================
c                                                                     *
c EPSP     = Effective plastic strain                                 *
c                                                                     *
c Material constants                                                  *
c props(1-3)  = Single crystal elastic constant, c11, c12, c44        *
c props(4)    = Whether Update Euler angles                           *
c props(5)   = Integration scheme:                                    *
c              + 100 - Modified Euler Forward Integration             *
c              + 200 - Grujicic&Batchu, Foread Euler (explicit)       *
c props(6)   = Maximum Gamma value on a slip system during one step   *
c             GammaDot(IS)*DT < props(6)                             *
c props(7)   = Error calculation method:                             *
c              + 10 - ||SIGDOT2-SIGDOT1||/||SIGDOT2+SIGDOT1||         *
c              + 20 - ||SIGDOT2-SIGDOT1||/||SIG2||                    *
c props(8)   = Error Torlerance value.                               *
c props(9)   = Substep time size guess value, n times of last time step
c props(10)   = Types of visco type flow laws:                        *
c              + 11:  Gamma=(Tau/GSS)**(1/M)                          *
c              + 22:  Gamma=(Tau/GSS)**(1/M) if Tau > GSS             *
c                       Else, Gamma = 0.0   
c props(11) = Whether allow change from ME to FE integartion          *
c props(12) = Minmum SumofGamma dot when change    (Recomend, 0,03)   *
c props(13) = Maximum difference of Lcur and Lpre  (Recomend, 0,01)   *
c props(14) = Minimun consitent ME with one steps number, (R, 50)     *
c props(15) = Number of FE steps (R,5)                                *
c props(16) = Number of ME steps (R,2)                                *
c props(17)   = Single crystal model number , and # of constants      *
c               1:  Peirce-Asaro-Needleman: 6                      
c               2:  Kalidindi:              7  
c               3:  Voce:                  4+2N 
c               4:  Generalized Voce        7
c               5:  Johson-cook             7           
c props(18)   = N for Voce Model only (N=1 or 2). for rest N=0     
c props(19-26) = Constants of the single crystal model
c               1: Gamd0, m, tau0, q,  h0,     taus
c               2: Gamd0, m, tau0, q,  h0,     a,    taus 
c               3: Gamd0, m, tau0, q,  theta1, tau1, (theta2, tau2)
c               4: Gamd0, m, tau0, q,  theta0, tau1, theta1
c               5: Gamd0, m, aa,   bb, cc,     nn,   eps0  
c                                                                     *
c History variables, stateold, statenew                               *
c SDV(1-3) = New Euler angles after update                            *
c SDV(4)   = Step counts, if 0, intialise F0 and R0 to unit tensor    *
c SDV(5-13)  = R tensor of last step in crystal basis, R0 is a unit   *
c SDV(14-25) = Critical Resolved shear stresses CRSS                  *
c SDV(26-37) = Absolute value of slips' GAMMA, for hardening          *
c SDV(38)    = Effective plastic strain, EPSP           *
c SDV(40)    = Average time stp                                       *
c SDV(41)    = Number of substeps of last step                        *
c SDV(42)    = Plastic work                                           *
c SDV(43)    = Consistent numbers of ME substeps with only 1 substep  *
c SDV(44)    = Whether allow change from ME scheme to FE scheme       *
c SDV(45)    = Number of FE steps after changing                      *
c SDV(46)    = Number of ME steps when FE and ME are alternated       *
c SDV(48)    = Total number of forward steps                          *
c SDV(49-57) = Global velocity gradient tensor; LG11,12,13            * 
c              LG21,22,23,31,32,33                                    *
c                                                                     *
c======================================================================
#IFDEF STANDALONE  
#ELSE
c     ===>> -----------------------------------------------------------
      subroutine VUMAT(
c     READ ONLY - DO NOT MODIFY
     . nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal, steptime,
     . totaltime, dt, cmname, coordmp, charlength, props, density,
     . straininc, relspininc, tempold, stretchold, defgradold, fieldold,
     . stressold, stateold, enerinternold, enerinelasold, tempnew,
     . stretchnew, defgradnew, fieldnew,
c     WRITE ONLY - DO NOT READ
     . stressnew, statenew, enerinternnew, enerinelasnew)
c
      include 'VABA_PARAM.INC'
c
      character *80 cmname
      dimension props(nprops), density(nblock), coordmp(nblock,*),
     . charlength(nblock), straininc(nblock,ndir+nshr), 
     . relspininc(nblock,nshr), tempold(nblock),
     . stretchold(nblock,ndir+nshr), 
     . defgradold(nblock,ndir+nshr+nshr), fieldold(nblock,nfieldv),
     . stressold(nblock,ndir+nshr), stateold(nblock,nstatev),
     . enerinternold(nblock),  enerinelasold(nblock), tempnew(nblock),
     . stretchnew(nblock,ndir+nshr), defgradnew(nblock,ndir+nshr+nshr),
     . fieldnew(nblock,nfieldv), stressnew(nblock,ndir+nshr) ,
     . statenew(nblock,nstatev), enerinternnew(nblock),
     . enerinelasnew(nblock)
c     <<=== -----------------------------------------------------------      
#ENDIF
c----------------------- INTERNAL VUMAT VARIABLES ---------------------
      integer, parameter  ::  nssgr = 7,
c                             number of slip system classes
     +                        Nsmax = 200
c                             max number of slip systems in total 
      INTEGER I, J, IS, NTYP, ITEALG, NB, cs, Nslips, Nslipsgr(nssgr)
      real(8), dimension(6) :: Svec, Sveci,Svec_old, SDOT, Dvec, Cdiag,
     +                         ERRM
      real(8), dimension(3,3) :: Smat, Fold, Fnew, DF, Uold, Unew,
     +  RA1, RA2, RC, QC, L, Dn, Wn, WE, DR      
      real(8), dimension(12) :: TauC, GAMMADOT, GammaA
      real(8) :: C11, C12, C44, STEPF, dt0, CMOD(14), ANGCUR(3),
     + SYMSCHM(5,12), SKWSCHM(3,12), EPSP, DSSE , PlasWorkAll, PLSWORK, 
     + inner, vdot, TEnergy_inc 
      common /Initials/   SYMSCHM, SKWSCHM
c      
c     Key variable for Modified Euler Integration
      real(8), dimension(6) :: Svec_1, Svec_2, SDOT_1, SDOT_2
      real(8), dimension(3,3) :: L_old, WE_1, WE_2, TMP
      real(8), dimension(12) :: TauC_1, TauC_2,TCDOT, TCDOT_1,TCDOT_2,
     + GAMMADOT_1, GAMMADOT_2, GammaA_1, GammaA_2
      real(8) :: DT_TMP, DELTAT, DTFACTOR, SGAMSUM_1,  TOL,
     + R_ERR, DTFACTOR_GUESS, NORM_ERR_S1, NORM_S2,
     + DTFGUESS_CONTROL, NORM_STRESS, MAXGMMMAPER, PLSWORK_1,PLSWORK_2,
     + MF_MAXGAMMA, MF_LDIF,RFF_1, RFF_2 , EPSP_1, EPSP_2, FF, FF_1
      INTEGER INTS, SUBSTEPNR, MF_FENR,MF_MENR,
     + ERR_MODE, SUBSTEPNR_LAST, FLAG, FLOWRULE, MF_FLAG, MF_CMENUM  
c     MF_FLAG: Whether allow change from ME to FE
c     MF_INFEFLAG: Change from ME into FE flag
c     MF_CMENUM: Consistent number of ME with 1 step
      logical :: full  = .FALSE.
      logical :: FirstCall
      DATA       FirstCall /.TRUE./
c
c ----------- PRINT MATERIAL PARAMETERS FIRST CALL OF UMAT ------------
c     Runs only once during the whole simulation          
      IF ((FirstCall) .AND.  (totaltime .GT. 1d-16)) THEN
          write(6,200) (PROPS(i),i=1,26)
          FirstCall = .FALSE.
c ---------------------- INITIALIZE SLIP SYSTEMS ----------------------      
c     Schmid tensors and Symmetric and antisymmetric part
ccc      CALL SCHMID(SYMSCHM,SKWSCHM)
      cs=1000000
      CALL init_slipsystems(cs, full, Nslipsgr, Nslips, 
     +                      Nsmax, nssgr, SYMSCHM, SKWSCHM)
c 
c     Nslipsgr -  Number of slip systems in each slip class
c     Nslips -    total Number of active slip systems         
c
      ENDIF
c
c ---------- Read elastic constants from ABAQUS material card ---------
c      
      C11  = props(1)
      C12  = props(2)
      C44  = props(3)
      Cdiag = [C11 + 2.d0*C12, C11 - C12, C11 - C12, 
     +         2.d0*C44, 2.d0*C44, 2.d0*C44]
c 
c ---------------------------------------------------------------------      
      IF (totaltime .LE. 1.d-20) THEN    
c     =======================       Time = 0        ===================
c     Assuming small elastic deformation, for the calculation of initial
c     elastic wave speeds          
          DO NB=1,NBLOCK  
c     
          stressnew(NB,1) = stressold(NB,1)+C11*straininc(NB,1)
     +                 +C12*straininc(NB,2)+C12*straininc(NB,3)
          stressnew(NB,2) = stressold(NB,2)+C12*straininc(NB,1)
     +                 +C11*straininc(NB,2)+C12*straininc(NB,3)
          stressnew(NB,3) = stressold(NB,3)+C12*straininc(NB,1)
     +                 +C12*straininc(NB,2)+C11*straininc(NB,3)
          stressnew(NB,4) = stressold(NB,4)+C44*straininc(NB,4)*2.0
          stressnew(NB,5) = stressold(NB,5)+C44*straininc(NB,5)*2.0
          stressnew(NB,6) = stressold(NB,6)+C44*straininc(NB,6)*2.0
          ENDDO !Block Loop
      ELSE
c     =======================       Time > 0        ===================
          DO NB=1,NBLOCK   
c	-----------------------------------------------------------------  
c	-------------------- Main Program-------------------------------- 
c	----------------------------------------------------------------- 
c     -01 ===>> Define materials constants ----------------------------
c     Whether update Euler angles every step           ((1))
      NTYP = props(4)
c     Integration scheme                               ((100))
      ITEALG=props(5)
c     Maximum Gamma value for per substep              ((0.25))
      MAXGMMMAPER = props(6)
c     Methods of calculating error value. 10/20        ((20))
      ERR_MODE = props(7)  
c     Error Torlerance value                           ((1.d-4))
      TOL = props(8)  
c     Substep time size control                        ((2))
      DTFGUESS_CONTROL = props(9)  
      IF(DTFGUESS_CONTROL<1.D-3) DTFGUESS_CONTROL = 1.D7 
c     Single crystal flow laws type                    ((11))
      FLOWRULE = props(10)
c     Read ME_2_FE control parameters     ((0, 0.03, 0.01, 50, 5, 2))
      MF_FLAG =     props(11)
      MF_MAXGAMMA = props(12)
      MF_LDIF =     props(13)
      MF_CMENUM =   props(14)
      MF_FENR =     props(15)
      MF_MENR =     props(16)       
c      
c     Read Single crystal plasticity model	((3))   
      CMOD(1)=props(17)     	
c     Read N in Voce Model                    ((1))
      CMOD(2)=props(18)
      DO I=1,8
          CMOD(2+I)=props(18+I)
      ENDDO	   
c
c     -01 <<===--------------------------------------------------------
c      
c     -02 ===>> Integration point initialization ----------------------
c ------- INITIALIZATION OF SVDS ONLY FOR THE "ZERO" INCREMENT --------              
c     Executed for every IP only once at the beginning, Every SDV that 
c     needs to have a non-zero value at t=0 should be initialized here.          
      IF ((totaltime - dt) .LE. 1d-20) THEN  
c         Initialize and store Step in REAL  
          stateold(NB,4) = 1.D0
c        
c         initial orientation matrix QC, generated with initial euler angles
          CALL ORIENTSL(stateold(NB,1:3), QC)
c         store initial crystal rotation matrix  RC
          CALL MTRANSP(QC,3,RC)
          stateold(NB,5:13) = reshape(RC, [9])  
c
c         Initialize and store critial resolved shear stress values
          DO IS=1,12
              stateold(NB,13+IS) = CMOD(5)
          ENDDO
c 	   
      ENDIF ! Increment Zero
c     -02 <<===-------------------------------------------------------- 
c     
c     -03 ===>> Load History Variables --------------------------------	  
c     Step in REAL  
      STEPF = stateold(NB,4)
c  
c     Critical Resolved shear stresses CRSS
      DO IS=1,12
	    TauC(IS) = stateold(NB,13+IS)
      ENDDO	  
c     Absolute slip gamma from last step 	
      DO IS=1,12
          GammaA(IS)=stateold(NB,25+IS)
      ENDDO  
c     Substep number of last step
      SUBSTEPNR_LAST = stateold(NB,41) 
c     Plastic work
      PlasWorkAll=stateold(NB,42)
      EPSP=stateold(NB,38) ! Hassan  
c     -03 <<===--------------------------------------------------------
c 
c------------------------ Bulding Rotaion Matrixes  ------------------- 
c     Old & New Deformation Gradient, F
      CALL AV2MAT(defgradold(NB,:),Fold,9)
      CALL AV2MAT(defgradnew(NB,:),Fnew,9)     
c     Old and new Stretch tensor, U
      CALL AV2MAT(stretchold(NB,:),Uold,6)  
      CALL AV2MAT(stretchnew(NB,:),Unew,6)    
c                  
c------ Rotation Matrix in F1=RA1.U1 (Abaqus > Global) transformation --
c
      CALL MINV3(Uold)
      CALL mmult(Fold, Uold, 3, RA1)
c  
c------ Rotation Matrix in F2=RA2.U2 (Global > Abaqus) transformation --          
c 
      CALL MINV3(Unew)
      CALL mmult(Fnew, Unew, 3, RA2)
c          
c ------- VELOCITY GRADIENT, L AT N+1/2 TIME STEP (GLOBAL SYSTEM) -----  
c     Second-order scheme
      DF = (Fnew-Fold)*0.5d0/dt
      CALL MINV3(Fold)
      CALL MINV3(Fnew)
      CALL mmult(DF, Fnew+Fold, 3, L) 
c
c     Alternatively: first-order forward-Euler scheme
c     DF = (Fnew-Fold)*1.d0/dt
c     CALL MINV3(Fold)
c     CALL mmult(DF, Fold, 3, L)      
c
c --------- RATE-OF-DEFORMATION AND SPIN, D & W (GLOBAL SYSTEM) -------
      CALL getsym(L, 3, Dn)      
c     the total spin tensor   
      CALL getskw(L, 3, Wn)
c
c---------- Rotation Matrix For (Global <> Crystal) transformation ----
c
      RC = reshape(stateold(NB,5:13), [3,3])  
c       
c--------------------------- Transformations -------------------------- 
c          
c     Old stress tensor in Abaqus Co-rotational framework  
      CALL AV2MAT(stressold(NB,:),Smat,6)   
c     -------  (Abaqus  >>  Global)  -------  
c     Cauchy stress is already pre-rotated by RA1 by Abaqus
c     and this needs to be undone 
      CALL TRANSFORMB(Smat, RA1, 3, Smat)
c     -------- (Global  >> Crystal)  ------- 
c     Rotate S into the crystal system given by RC(n) at time t(n) 
      CALL TRANSFORM(Smat, RC, 3, Smat)     
c     Rotate Strain Increment into the crystal system:
      CALL TRANSFORM(Dn, RC, 3, Dn)
c     Rotate Total spin increment into the crystal system: 
      CALL TRANSFORM(Wn, RC, 3, Wn)  
c
c     Convert S and E into the Natural Notation          
      CALL MAT2NN(Smat,Svec)
      CALL MAT2NN(Dn,Dvec)  
      Svec_old=Svec
c          
c     -----------------------------------------------------------------
c     --------------- Crystal Plasticity Part  ------------------------	
c     ----------------------------------------------------------------- 
c     Input CMOD values
      CMOD(11) = MAXGMMMAPER
      CMOD(12) = STEPF
c     CMOD(13) = 0  -- Current substeps number
      CMOD(14) = FLOWRULE	  
c
      dt0=dt
      SUBSTEPNR = 0
      PLSWORK = 0.D0
c      
c     --------- New feature: whether change From ME to FE -------
      INTS = stateold(NB,44)
      IF((INTS==1).AND.(MF_FLAG==1)) THEN
c         Check the difference of current L and Last step
          L_old(1,1) = stateold(NB,49)
          L_old(1,2) = stateold(NB,50)
          L_old(1,3) = stateold(NB,51)
          L_old(2,1) = stateold(NB,52)
          L_old(2,2) = stateold(NB,53)
          L_old(2,3) = stateold(NB,54)
          L_old(3,1) = stateold(NB,55)
          L_old(3,2) = stateold(NB,56)
          L_old(3,3) = stateold(NB,57)
c         Calculate the error
          DO I=1,3
              DO J=1,3
                  TMP(I,J)=L(I,J)-L_old(I,J)
              ENDDO 
          ENDDO
          CALL MNORM(TMP,3,3,RFF_1)
          CALL MNORM(L,3,3,FF)
          CALL MNORM(L_old,3,3,FF_1)
          RFF_2 = RFF_1*100.0/(FF_1*100.D0+FF*100.D0+1.D-8)
c         Last time step
          FF = stateold(NB,40)	   
c         When error of L is smaller than defined; current time step is
c         smaller than last one
570       IF((RFF_2<MF_LDIF).AND.(dt < 1.1*FF)) THEN
              I = stateold(NB,45)
c             If FE steps are smaller than the set values
              IF(I.LT.MF_FENR) THEN
                  ITEALG = 200
                  statenew(NB,45) = stateold(NB,45) + 1
                  statenew(NB,48) = stateold(NB,48) + 1 
c                 WRITE(11,*) STEPF, 200
              ElSE 
                  statenew(NB,46) = stateold(NB,46) + 1
c                 WRITE(11,*) STEPF, 100
		        IS = stateold(NB,46)
                  IF(IS.GE.MF_MENR) THEN
                      statenew(NB,45) = 0
                      statenew(NB,46) = 0
                  ENDIF
              ENDIF 
c             Else, reset the ME2FE values
571       ELSE
              statenew(NB,43) = 0
              statenew(NB,44) = 0
              statenew(NB,45) = 0
              statenew(NB,46) = 0             
          ENDIF	
      ENDIF 
c
c     
      SELECT CASE(ITEALG)      
      CASE(100)
c     -----------------------------------------------------------------
c     --------------- Modified Euler Algorithm  -----------------------	
c     ME ===>>> -------------------------------------------------------
c	 
      DELTAT = 0.D0
      DTFACTOR = 1.D0
      SUBSTEPNR = 0
c     Guess the possible substep time size, using last step's substepping number
      FF_1 = SUBSTEPNR_LAST
      IF(FF_1<0.999999) FF_1 = 1.0D0
c     Average substepping time size of last step
	FF = 1.D0/FF_1
c     Guess the substepping time size	  
      DTFACTOR_GUESS = DTFGUESS_CONTROL*FF       
      IF(DTFACTOR_GUESS>=1.0D0) THEN
          DTFACTOR_GUESS = 1.0D0
      ENDIF 
      DTFACTOR = DTFACTOR_GUESS	  
c	
60    DO WHILE((DELTAT-1.0)<-1.D-14)
          CMOD(13) = SUBSTEPNR	   
62        DO WHILE(1)
              DT_TMP = dt0*DTFACTOR
              IF(DTFACTOR<1.D-12 )THEN
                  WRITE(*,*) 'DTFACTOR IS TOO SMALL,' , DTFACTOR
                  PAUSE		
              ENDIF	  
c
c             ----------------- The First ME STEP  --------------------	
c             ME1 ===>>> ----------------------------------------------  
              WE_1 =   0.D0
              Svec_1 = Svec
              SDOT_1 = 0.D0
              TauC_1 = TauC
              GammaA_1= GammaA
              TCDOT_1 = 0.D0
              GAMMADOT_1 = 0.D0
              EPSP_1 = EPSP
c	     
              FLAG = 10 !Indicts it's the first ME step
              CALL SOLVER(Svec_1,Dvec,Wn,RC,TauC_1,GammaA_1,
     +             Cdiag,GAMMADOT_1, SDOT_1,DT_TMP,CMOD, WE_1, 
     +             PLSWORK_1, EPSP_1, FLAG, SKWSCHM, SYMSCHM,1)
c             Compute rate of internal variables of step 1
              DO IS=1,12
                  TCDOT_1(IS) = (TauC_1(IS)-TauC(IS))/DT_TMP
              ENDDO
c             If return value indicts the time step is too large from second step; change time step and cycle
              IF(FLAG==-10) THEN
                  DTFACTOR = DTFACTOR*0.5D0
                  CYCLE
              ENDIF    
c             ----------------- The Second ME STEP  -------------------	
c             ME2 ===>>> ----------------------------------------------
              WE_2 =   0.D0
              Svec_2 = Svec_1
              SDOT_2 = 0.D0
              TauC_2 = TauC_1
              GammaA_2= GammaA_1
              TCDOT_2 = 0.D0
              GAMMADOT_2 = 0.D0
              EPSP_2 = EPSP_1
c
              CALL SOLVER(Svec_2,Dvec,Wn,RC,TauC_2,GammaA_2,
     +             Cdiag,GAMMADOT_2,SDOT_2, DT_TMP,CMOD, WE_2, 
     +             PLSWORK_2, EPSP_2, FLAG, SKWSCHM, SYMSCHM,2)
c             Compute the rate of internal variables from step2
              DO IS=1,12
                  TCDOT_2(IS) = (TauC_2(IS)-TauC_1(IS))/DT_TMP
              ENDDO
c             If return value indicts the time step is too large from second step; change time step and cycle
              IF(FLAG==-20) THEN
                  DTFACTOR = DTFACTOR*0.5D0
                  CYCLE
              ENDIF
c             ------------------- Updating Stress ---------------------
              SDOT = (SDOT_2 + SDOT_1)*0.5D0
c             hydrostatic pressure update
              Sveci(1) = Svec(1) + Cdiag(1)*Dvec(1)*DT_TMP
c             deviatoric Stress update               
              Sveci(2:) = Svec(2:) + SDOT(2:)*DT_TMP
c              
c             ------------- Compare and Assess substepping  ----------- 
c    
              ERRM = SDOT_2-SDOT_1
              NORM_ERR_S1=DSQRT(vdot(ERRM,ERRM,6))              
              IF(ERR_MODE==10) THEN
                  ERRM = SDOT_2+SDOT_1
                  NORM_S2=DSQRT(vdot(ERRM,ERRM,6))
	            R_ERR = NORM_ERR_S1/(NORM_S2+1.D-8)             
              ELSEIF (ERR_MODE==20) THEN
                  NORM_STRESS=DSQRT(vdot(Svec,Svec,6))
	            R_ERR = NORM_ERR_S1*DT_TMP/(NORM_STRESS+1.D-8)
              ELSE
	            PAUSE 'Unrecognised Error Control Method!'
              ENDIF
c             Check the error
              IF((R_ERR-TOL)>1.D-13) THEN
c	            If too large error, reduce the time step and redo
		        FF = 0.8*DSQRT(TOL/R_ERR)
                  IF(FF>2.D0) FF = 2.D0
                  IF(FF<0.1D0) FF = 0.1D0
                  DTFACTOR = FF*DTFACTOR
              ELSE
c	            Else, exit and update the values
                  EXIT
              ENDIF 	    	   	  
c	  
63        ENDDO
c         ------------ Update variables after substepping  ------------	
c         UPDATE ===>>> -----------------------------------------------
c	    --- Update time recording varible -------------------
          DELTAT = DELTAT + DTFACTOR
          SUBSTEPNR = SUBSTEPNR + 1
          IF((DELTAT-1.D0)>1.D-12) THEN
              DT_TMP = (1.D0+DTFACTOR-DELTAT)*dt0
              DELTAT = 1.D0
          ENDIF
c    
c         ---- Update stress ----------------------------------
          Svec = Sveci
c	    ---- Update RC   --------------------------------------
          WE = (WE_1 + WE_2)*0.5D0
c
          CALL mmult(WE, WE, 3, DR)
          DR = DT_TMP/(1.d0+(DT_TMP/2.d0)**2
     +          * inner(WE,WE))*(WE+(DT_TMP/2.d0)*DR)
          DO i=1,3
              DR(i,i) = DR(i,i) + 1.d0
	    ENDDO
c         Update of the crystal orientation matrix
          CALL mmult(RC, DR, 3, TMP)
          RC=TMP
c           
c         Normalised RC
          CALL NORMALMAT(RC) 
c          
c	    ------ Update Euler Angles -------------------------
          CALL MTRANSP(RC,3,QC)
c	    For texture o
          IF(NTYP) CALL EULERU44(QC,ANGCUR)         
c
c         Von-Mises plastic strain   
          EPSP = EPSP + (EPSP_1-EPSP)*0.5D0 + (EPSP_2-EPSP_1)*0.5D0    
c
c	    --- Update PLSTAS,GAMMA of slip systems ----
          DO IS=1,12
              GAMMADOT(IS) = (GAMMADOT_1(IS)+GAMMADOT_2(IS))*0.5D0
              GammaA(IS) = GammaA(IS) + 
     +        DABS(GAMMADOT(IS)*DT_TMP)
          ENDDO 
c	  
c	    --- Update critical resovled shear stress & Plastic work --------------
          DO IS=1,12
              TCDOT(IS) = (TCDOT_1(IS)+TCDOT_2(IS))*0.5D0      
              TauC(IS) = TauC(IS) + TCDOT(IS)*DT_TMP
          ENDDO
c         ------- Plastic work recording ----------
          PLSWORK = PLSWORK + (PLSWORK_1+PLSWORK_2)*0.5D0	  
c       
61    ENDDO
c
c     ME <<<=== -------------------------------------------------------
      CASE(200)
c     -----------------------------------------------------------------
c     --------------- Forward Euler Algorithm  ------------------------	
c     FE ===>>> -------------------------------------------------------
c	 
      FLAG = 10 !Indicts it's the first ME step
      CMOD(13) = 0
      CALL SOLVER(Svec, Dvec, Wn, RC,
     + TauC, GammaA, Cdiag, GAMMADOT,SDOT, 
     + dt0, CMOD, WE, PLSWORK, EPSP, FLAG, SKWSCHM, SYMSCHM,1)
      IF(FLAG==-10) THEN
          WRITE(*,*) 'First step is not purely elastic!'
          PAUSE
      ENDIF
c     ---- Update RC   --------------------------------------
      CALL mmult(WE, WE, 3, DR)
      DR = dt0/(1.d0+(dt0/2.d0)**2 * inner(WE,WE))*(WE+(dt0/2.d0)*DR)
      DO i=1,3
          DR(i,i) = DR(i,i) + 1.d0
      ENDDO
c     Update of the crystal orientation matrix
      CALL mmult(RC, DR, 3, TMP)
      RC=TMP
c           
c     Normalised RC
      CALL NORMALMAT(RC)       
c	------ Update Euler Angles -----------------------------
      CALL MTRANSP(RC,3,QC)
c	For texture o
      IF(NTYP) CALL EULERU44(QC,ANGCUR)
c      
c     FE <<<=== ------------------------------------------------------- 		   
      END SELECT
c     
c--------------------------- Transformations --------------------------
c       
      CALL NN2MAT(Svec,Smat)      
c     ------- (Crystal >>  Global)  ------- Only Stress 
      CALL TRANSFORMB(Smat, RC, 3, Smat) 
c     -------  (Global  >> Abaqus)  ------- Only Stress
      CALL TRANSFORM(Smat, RA2, 3, Smat)
c      
c     Convert S into the Abaqus-Voigt (explicit) vector notation
c     Storing updated stress       
      CALL MAT2AV(Smat,stressnew(NB,:),6)    
c  
c     -----------  Record Variables in History vairiabls  -------------
c     Step in REAL  
   	STEPF = STEPF + 1.D0
   	statenew(NB,4) = STEPF
c      
c	Store updated orientation of the crystal, R(n+1)
      statenew(NB,5:13) = reshape(RC, [9])      
c       
c     Record critial resolved shear stress values
      DO IS=1,12
          statenew(NB,13+IS) = TauC(IS)
      ENDDO	 
c     Record slip Gammas
      SGAMSUM_1 = 0.D0
      DO IS=1,12
          statenew(NB,25+IS) = GammaA(IS)
          SGAMSUM_1 = SGAMSUM_1 + GammaA(IS)
      ENDDO
c     Record new Euler angles
   	statenew(NB,1) = ANGCUR(1)
   	statenew(NB,2) = ANGCUR(2)
   	statenew(NB,3) = ANGCUR(3)
c     Record substep number
      IF (SUBSTEPNR>0) THEN
          statenew(NB,40) = dt*1.0/SUBSTEPNR
      ELSE
          statenew(NB,40) = dt
      ENDIF
      statenew(NB,41) = SUBSTEPNR
c     Recrod palstic work
      PlasWorkAll=PlasWorkAll + PLSWORK
      statenew(NB,42)=PlasWorkAll 	 
      statenew(NB,38)=EPSP ! Hassan
c     ---------------- New Feature in the V2,2 Veriosn ----------------
c     For Changing from ME into FE. 
      INTS = props(5)
      IF((SUBSTEPNR<2).AND.(INTS==100)) THEN
          IF(SUBSTEPNR == 1) statenew(NB,43) = stateold(NB,43) + 1	    
      ELSE
          statenew(NB,43) = 0
          statenew(NB,44) = 0
          statenew(NB,45) = 0
          statenew(NB,46) = 0	   
      ENDIF
c
c     Check whether allow for ME to FE change
      I = stateold(NB,43)
      IF((SGAMSUM_1>MF_MAXGAMMA).AND.(I>MF_CMENUM)) THEN
          statenew(NB,44) = 1        
      ENDIF 
c -----------------------------------------------------  
c
c	Get total L in last step, (step n)
      statenew(NB,49) = L(1,1)
      statenew(NB,50) = L(1,2)
      statenew(NB,51) = L(1,3)
      statenew(NB,52) = L(2,1)
      statenew(NB,53) = L(2,2)
      statenew(NB,54) = L(2,3)
      statenew(NB,55) = L(3,1)
      statenew(NB,56) = L(3,2)
      statenew(NB,57) = L(3,3)
c
c     
c ------------ UPDATE THE DISSIPATED PLASTIC AND INTERNAL ENERGY ------
c     
c     Increment of Total internal energy          
      TEnergy_inc= vdot(svec_old+svec, Dvec*dt0, 6)/2.d0    
c     Internal energy per unit mass 
      enerInternNew(NB)= enerInternOld(NB) + TEnergy_inc/density(NB)
c     Increment of Elastic energy
      DSSE = 0.d0
      DO i=1, 6
          DSSE = DSSE + 1.d0/Cdiag(i)
     +          * (svec(i)+svec_old(i))*(svec(i)-svec_old(i))/2.d0
      ENDDO   
c     Inelastic energy per unit mass
      enerInelasNew(NB) = enerInelasold(NB) +
     +                    (TEnergy_inc - DSSE)/density(NB)
          ENDDO !Block Loop
      ENDIF     !Time > 0 
c
      return
c      
200   format(/
     +        5x, '***********************************'/,
     +        5x, '***********************************'/,
     +        5x, '           Rate-dependent          '/,
     +        5x, '      crystal plasticity with      '/,
     +        5x, '         substepping scheme        '/,
     +        5x, '                                   '/,
     +        5x, '              WITH                 '/,
     +        5x, '      MATERIAL PARAMETERS:         '/,
     +        5x, '                                   '/,
     +        5x, ' Elasticity:                       '/,
     +        5x, ' -- C11      = ',            1pe12.5/,
     +        5x, ' -- C12      = ',            1pe12.5/,
     +        5x, ' -- C44      = ',            1pe12.5/,
     +        5x, ' Update Euler angles?     = ', 1pe12.5/,
     +        5x, ' Integration scheme       = ', 1pe12.5/,
     +        5x, ' Max Gamma per subtesp    = ', 1pe12.5/,
     +        5x, ' Error control mode       = ', 1pe12.5/,
     +        5x, ' Error Tolerance value    = ', 1pe12.5/,
     +        5x, ' Substep timesize control = ', 1pe12.5/,
     +        5x, ' Flow law type = ',          1pe12.5/,
     +        5x, ' Integration control params:       '/, 
     +        5x, ' -- MF_FLAG    = ',          1pe12.5/,
     +        5x, ' -- MF_MAXGAMMA= ',          1pe12.5/,
     +        5x, ' -- MF_LDIF    = ',          1pe12.5/,
     +        5x, ' -- MF_CMENUM  = ',          1pe12.5/,
     +        5x, ' -- MF_FENR    = ',          1pe12.5/,
     +        5x, ' -- MF_MENR    = ',          1pe12.5/, 
     +        5x, ' Hardening and Material Constants: '/,
     +        5x, ' Hardening Law = ',          1pe12.5/, 
     +        5x, ' -- N (Voce)   = ',          1pe12.5/,
     +        5x, ' -- Gamma_dot0 = ',          1pe12.5/,
     +        5x, ' -- m          = ',          1pe12.5/,
     +        5x, ' -- tau0       = ',          1pe12.5/,
     +        5x, ' -- xx         = ',          1pe12.5/,
     +        5x, ' -- xx         = ',          1pe12.5/,
     +        5x, ' -- xx         = ',          1pe12.5/,
     +        5x, ' -- xx         = ',          1pe12.5/,
     +        5x, ' -- xx         = ',          1pe12.5/)
c
c
      end subroutine VUMAT  
c
c      
c----------------------------------------------------------------------
c     ----------------:: Subroutines and Functions ::------------------ 
c----------------------------------------------------------------------
c 
c     ================================================================= 
c     ================================================================= 
      subroutine init_slipsystems(cs, full, active, Nslips, Nsmax, 
     +                            nssgr, P, Omega)
c     Subroutine defines slip systems, build Schmid matrix, its 
c     symmetric part "P" and skew part "Omega"
c    
c     Input: sizes - specifies the set of slip systems used for the 
c                    calculations as:
c
c          FCC
c     '(111) <1-10>'
c     '(110) <1-10>' 
c     '(100) <1-10>'
c     '(112) <1-10>'
c         
c          BCC
c     '(101) <11-1>'
c     '(112) <11-1>'
c     '(123) <11-1>'
c
      implicit none
c
c     number of slip systems in each of the slip classes if both positive
c     and negative directions are counted    
      integer, parameter, dimension(7) 
     +                    :: sizes = [24, 12, 12, 24, 24, 24, 48]
      real(8), parameter  :: inv3  = 1.d0/sqrt(3.d0),
     +                       inv2  = 1.d0/sqrt(2.d0),
     +                       inv6  = 1.d0/sqrt(6.d0),
     +                       inv14 = 1.d0/sqrt(14.d0)
c
c     input
      integer :: cs, Nsmax, nssgr
      logical :: full
c     output
      integer :: Nslips, active(nssgr)
      real(8) :: P(5,Nsmax), Omega(3,Nsmax)
c     
      integer :: i, s, r, k, Nsizes, csvec(nssgr), active_temp(nssgr)
      real(8) :: n(Nsmax,3), b(Nsmax,3), schmid(3,3,Nsmax), 
     +           schmid_sym(3,3,Nsmax), schmid_skw_vec(3,Nsmax),
     +           schmid_skw(3,3,Nsmax), schmid_sym_vec(5,Nsmax),
     +           tmp(6)
c  
      Nsizes = sum(sizes)
      do i=1, nssgr 
          csvec(nssgr+1-i) = mod(cs, 10)
          cs = (cs - mod(cs, 10))/10
      end do
c     number of slip systems in each slip class required
      active_temp = csvec*sizes
c     slip systems described by slip plane normal "n" and slip direction "b"
c     slip plane normals "n"
c     cs = 1000000 (FCC octahedral slips)
      n(1,:)  = [ 1.d0,  1.d0, -1.d0]
      n(2,:)  = [ 1.d0,  1.d0, -1.d0]
      n(3,:)  = [ 1.d0,  1.d0, -1.d0]
      n(4,:)  = [ 1.d0, -1.d0, -1.d0]
      n(5,:)  = [ 1.d0, -1.d0, -1.d0]
      n(6,:)  = [ 1.d0, -1.d0, -1.d0]
      n(7,:)  = [ 1.d0, -1.d0,  1.d0]
      n(8,:)  = [ 1.d0, -1.d0,  1.d0]
      n(9,:)  = [ 1.d0, -1.d0,  1.d0]
      n(10,:) = [ 1.d0,  1.d0,  1.d0]
      n(11,:) = [ 1.d0,  1.d0,  1.d0]
      n(12,:) = [ 1.d0,  1.d0,  1.d0]
c     cs = 0100000
      n(13,:) = [ 1.d0,  1.d0,  0.d0]
      n(14,:) = [ 1.d0, -1.d0,  0.d0]
      n(15,:) = [ 1.d0,  0.d0,  1.d0]
      n(16,:) = [ 1.d0,  0.d0, -1.d0]
      n(17,:) = [ 0.d0,  1.d0,  1.d0]
      n(18,:) = [ 0.d0,  1.d0, -1.d0]
c     cs = 0010000
      n(19,:) = [ 1.d0,  0.d0,  0.d0]
      n(20,:) = [ 1.d0,  0.d0,  0.d0]
      n(21,:) = [ 0.d0,  1.d0,  0.d0]
      n(22,:) = [ 0.d0,  1.d0,  0.d0]
      n(23,:) = [ 0.d0,  0.d0,  1.d0]
      n(24,:) = [ 0.d0,  0.d0,  1.d0]
c     cs = 0001000
      n(25,:) = [-1.d0,  1.d0,  2.d0]
      n(26,:) = [-1.d0,  1.d0, -2.d0]
      n(27,:) = [ 1.d0,  1.d0,  2.d0]
      n(28,:) = [ 1.d0,  1.d0, -2.d0]
      n(29,:) = [ 1.d0,  2.d0, -1.d0]
      n(30,:) = [ 1.d0, -2.d0, -1.d0]
      n(31,:) = [ 1.d0,  2.d0,  1.d0]
      n(32,:) = [ 1.d0, -2.d0,  1.d0]
      n(33,:) = [ 2.d0,  1.d0, -1.d0]
      n(34,:) = [-2.d0,  1.d0, -1.d0]
      n(35,:) = [ 2.d0,  1.d0,  1.d0]
      n(36,:) = [-2.d0,  1.d0,  1.d0]
c     cs = 0000100 (BCC base)
      n(37,:) = [ 1.d0,  1.d0,  0.d0]
      n(38,:) = [ 1.d0,  1.d0,  0.d0]
      n(39,:) = [ 1.d0, -1.d0,  0.d0]
      n(40,:) = [ 1.d0, -1.d0,  0.d0]
      n(41,:) = [ 0.d0,  1.d0,  1.d0]
      n(42,:) = [ 0.d0,  1.d0,  1.d0]
      n(43,:) = [ 0.d0, -1.d0,  1.d0]
      n(44,:) = [ 0.d0, -1.d0,  1.d0]
      n(45,:) = [ 1.d0,  0.d0,  1.d0]
      n(46,:) = [ 1.d0,  0.d0,  1.d0]
      n(47,:) = [ 1.d0,  0.d0, -1.d0]
      n(48,:) = [ 1.d0,  0.d0, -1.d0]
c     cs = 0000010 (BCC base)
      n(49,:) = [-1.d0,  1.d0,  2.d0]
      n(50,:) = [-1.d0,  1.d0, -2.d0]
      n(51,:) = [ 1.d0,  1.d0,  2.d0]
      n(52,:) = [ 1.d0,  1.d0, -2.d0]
      n(53,:) = [ 1.d0,  2.d0, -1.d0]
      n(54,:) = [ 1.d0, -2.d0, -1.d0]
      n(55,:) = [ 1.d0,  2.d0,  1.d0]
      n(56,:) = [ 1.d0, -2.d0,  1.d0]
      n(57,:) = [ 2.d0,  1.d0, -1.d0]
      n(58,:) = [-2.d0,  1.d0, -1.d0]
      n(59,:) = [ 2.d0,  1.d0,  1.d0]
      n(60,:) = [-2.d0,  1.d0,  1.d0]
c     cs = 0000001 (BCC)
      n(61,:) = [ 1.d0,  2.d0,  3.d0]
      n(62,:) = [-1.d0,  2.d0,  3.d0]
      n(63,:) = [ 1.d0, -2.d0,  3.d0]
      n(64,:) = [ 1.d0,  2.d0, -3.d0]
      n(65,:) = [ 1.d0,  3.d0,  2.d0]
      n(66,:) = [-1.d0,  3.d0,  2.d0]
      n(67,:) = [ 1.d0, -3.d0,  2.d0]
      n(68,:) = [ 1.d0,  3.d0, -2.d0]
      n(69,:) = [ 2.d0,  1.d0,  3.d0]
      n(70,:) = [-2.d0,  1.d0,  3.d0]
      n(71,:) = [ 2.d0, -1.d0,  3.d0]
      n(72,:) = [ 2.d0,  1.d0, -3.d0]
      n(73,:) = [ 2.d0,  3.d0,  1.d0]
      n(74,:) = [-2.d0,  3.d0,  1.d0]
      n(75,:) = [ 2.d0, -3.d0,  1.d0]
      n(76,:) = [ 2.d0,  3.d0, -1.d0]
      n(77,:) = [ 3.d0,  1.d0,  2.d0]
      n(78,:) = [-3.d0,  1.d0,  2.d0]
      n(79,:) = [ 3.d0, -1.d0,  2.d0]
      n(80,:) = [ 3.d0,  1.d0, -2.d0]
      n(81,:) = [ 3.d0,  2.d0,  1.d0]
      n(82,:) = [-3.d0,  2.d0,  1.d0]
      n(83,:) = [ 3.d0, -2.d0,  1.d0]
      n(84,:) = [ 3.d0,  2.d0, -1.d0]
c
c     slip directions "b"
c     cs = 1000000
      b(1,:)  = [ 0.d0,  1.d0,  1.d0]
      b(2,:)  = [ 1.d0,  0.d0,  1.d0]
      b(3,:)  = [ 1.d0, -1.d0,  0.d0]
      b(4,:)  = [ 0.d0,  1.d0, -1.d0]
      b(5,:)  = [ 1.d0,  0.d0,  1.d0]
      b(6,:)  = [ 1.d0,  1.d0,  0.d0]
      b(7,:)  = [ 0.d0,  1.d0,  1.d0]
      b(8,:)  = [ 1.d0,  0.d0, -1.d0]
      b(9,:)  = [ 1.d0,  1.d0,  0.d0]
      b(10,:) = [ 0.d0,  1.d0, -1.d0]
      b(11,:) = [ 1.d0,  0.d0, -1.d0]
      b(12,:) = [ 1.d0, -1.d0,  0.d0]
c     cs = 0100000
      b(13,:) = [ 1.d0, -1.d0,  0.d0]
      b(14,:) = [ 1.d0,  1.d0,  0.d0]
      b(15,:) = [ 1.d0,  0.d0, -1.d0]
      b(16,:) = [ 1.d0,  0.d0,  1.d0]
      b(17,:) = [ 0.d0,  1.d0, -1.d0]
      b(18,:) = [ 0.d0,  1.d0,  1.d0]
c     cs = 0010000
      b(19,:) = [ 0.d0,  1.d0,  1.d0]
      b(20,:) = [ 0.d0,  1.d0, -1.d0]
      b(21,:) = [ 1.d0,  0.d0,  1.d0]
      b(22,:) = [ 1.d0,  0.d0, -1.d0]
      b(23,:) = [ 1.d0,  1.d0,  0.d0]
      b(24,:) = [ 1.d0, -1.d0,  0.d0]
c     cs = 0001000
      b(25,:) = [ 1.d0,  1.d0,  0.d0]
      b(26,:) = [ 1.d0,  1.d0,  0.d0]
      b(27,:) = [ 1.d0, -1.d0,  0.d0]
      b(28,:) = [ 1.d0, -1.d0,  0.d0]
      b(29,:) = [ 1.d0,  0.d0,  1.d0]
      b(30,:) = [ 1.d0,  0.d0,  1.d0]
      b(31,:) = [ 1.d0,  0.d0, -1.d0]
      b(32,:) = [ 1.d0,  0.d0, -1.d0]
      b(33,:) = [ 0.d0,  1.d0,  1.d0]
      b(34,:) = [ 0.d0,  1.d0,  1.d0]
      b(35,:) = [ 0.d0,  1.d0, -1.d0]
      b(36,:) = [ 0.d0,  1.d0, -1.d0]
c     cs = 0000100 (BCC)
      b(37,:) = [ 1.d0, -1.d0,  1.d0]
      b(38,:) = [ 1.d0, -1.d0, -1.d0]
      b(39,:) = [ 1.d0,  1.d0,  1.d0]
      b(40,:) = [ 1.d0,  1.d0, -1.d0]
      b(41,:) = [ 1.d0, -1.d0,  1.d0]
      b(42,:) = [-1.d0, -1.d0,  1.d0]
      b(43,:) = [ 1.d0,  1.d0,  1.d0]
      b(44,:) = [-1.d0,  1.d0,  1.d0]
      b(45,:) = [ 1.d0,  1.d0, -1.d0]
      b(46,:) = [ 1.d0, -1.d0, -1.d0]
      b(47,:) = [ 1.d0,  1.d0,  1.d0]
      b(48,:) = [ 1.d0, -1.d0,  1.d0]
c     cs = 0000010 (BCC)
      b(49,:) = [ 1.d0, -1.d0,  1.d0]
      b(50,:) = [-1.d0,  1.d0,  1.d0]
      b(51,:) = [ 1.d0,  1.d0, -1.d0]
      b(52,:) = [ 1.d0,  1.d0,  1.d0]
      b(53,:) = [-1.d0,  1.d0,  1.d0]
      b(54,:) = [ 1.d0,  1.d0, -1.d0]
      b(55,:) = [ 1.d0, -1.d0,  1.d0]
      b(56,:) = [ 1.d0,  1.d0,  1.d0]
      b(57,:) = [ 1.d0, -1.d0,  1.d0]
      b(58,:) = [ 1.d0,  1.d0, -1.d0]
      b(59,:) = [-1.d0,  1.d0,  1.d0]
      b(60,:) = [ 1.d0,  1.d0,  1.d0]
c     cs = 0000001 (BCC)
      b(61,:) = [ 1.d0,  1.d0, -1.d0]
      b(62,:) = [ 1.d0, -1.d0,  1.d0]
      b(63,:) = [-1.d0,  1.d0,  1.d0]
      b(64,:) = [ 1.d0,  1.d0,  1.d0]
      b(65,:) = [ 1.d0, -1.d0,  1.d0]
      b(66,:) = [ 1.d0,  1.d0, -1.d0]
      b(67,:) = [ 1.d0,  1.d0,  1.d0]
      b(68,:) = [-1.d0,  1.d0,  1.d0]
      b(69,:) = [ 1.d0,  1.d0, -1.d0]
      b(70,:) = [ 1.d0, -1.d0,  1.d0]
      b(71,:) = [-1.d0,  1.d0,  1.d0]
      b(72,:) = [ 1.d0,  1.d0,  1.d0]
      b(73,:) = [ 1.d0, -1.d0,  1.d0]
      b(74,:) = [ 1.d0,  1.d0, -1.d0]
      b(75,:) = [ 1.d0,  1.d0,  1.d0]
      b(76,:) = [-1.d0,  1.d0,  1.d0]
      b(77,:) = [-1.d0,  1.d0,  1.d0]
      b(78,:) = [ 1.d0,  1.d0,  1.d0]
      b(79,:) = [ 1.d0,  1.d0, -1.d0]
      b(80,:) = [ 1.d0, -1.d0,  1.d0]
      b(81,:) = [-1.d0,  1.d0,  1.d0]
      b(82,:) = [ 1.d0,  1.d0,  1.d0]
      b(83,:) = [ 1.d0,  1.d0, -1.d0]
      b(84,:) = [ 1.d0, -1.d0,  1.d0]
c
      n(1:12,:)  = n( 1:12,:)*inv3
      n(13:18,:) = n(13:18,:)*inv2
      n(25:36,:) = n(25:36,:)*inv6
      n(37:48,:) = n(37:48,:)*inv2
      n(49:60,:) = n(49:60,:)*inv6
      n(61:84,:) = n(61:84,:)*inv14
      b(1:36,:)  = b( 1:36,:)*inv2
      b(37:84,:) = b(37:84,:)*inv3
c
      do i=1, Nsizes
          CALL outer(b(i,:), n(i,:), 3, schmid(:,:,i))
          CALL getsym(schmid(:,:,i), 3, schmid_sym(:,:,i))
          CALL MAT2NN(schmid_sym(:,:,i), tmp)
          schmid_sym_vec(:,i) = tmp(2:)
          CALL getskw(schmid(:,:,i), 3, schmid_skw(:,:,i))
          schmid_skw_vec(:,i) = [schmid_skw(2,3,i),
     +                           schmid_skw(1,3,i),
     +                           schmid_skw(1,2,i)]
      end do
c
      k = 1
      r = 1
      do i=1, nssgr
          s = active_temp(i)/2
          if (s .NE. 0) then
              P(:,k:k-1+s)         =  schmid_sym_vec(:,r:r-1+s)
              Omega(:,k:k-1+s)     =  schmid_skw_vec(:,r:r-1+s)
              if (full) then
                  P(:,k+s:k-1+2*s)     = -schmid_sym_vec(:,r:r-1+s)
                  Omega(:,k+s:k-1+2*s) = -schmid_skw_vec(:,r:r-1+s)
                  k = k + 2*s
              else
                  k = k + s
              end if
          end if
          r = r + sizes(i)/2
      end do
c
      if (full) then 
	      active = active_temp
	  else
	      active = active_temp/2
	  end if
      Nslips = sum(active)
c
      return
      end subroutine init_slipsystems  
c     =================================================================
c     =================================================================
!C**********************************************************************
!C                         SUBROUTINE SCHMID                           *
!C**********************************************************************
!C    Computes the Schmid tensor                                       *
!C**********************************************************************
      SUBROUTINE SCHMID(schmid_sym_vec,schmid_skw_vec)
!C
      IMPLICIT NONE
!C
      REAL*8 RM0(3,12),RN0(3,12),SCHM(3,3,12),S2,S3,
     +       schmid_sym(3,3,12), schmid_sym_vec(5,12), tmp(6) ,
     +       schmid_skw(3,3,12), schmid_skw_vec(3,12) 
      INTEGER I,J,IS
!C
!C Definition of the slip systems in plastic configuration Cp
!C Slip direction m and slip plane normal n
      S2=DSQRT(2.D0)
      S3=DSQRT(3.D0)
      IF(0) THEN	  
!C A2
      RN0(1,1)=-1.D0/S3
      RN0(2,1)=1.D0/S3
      RN0(3,1)=1.D0/S3
      RM0(1,1)=0.D0
      RM0(2,1)=-1.D0/S2
      RM0(3,1)=1.D0/S2
!C A3
      RN0(1,2)=-1.D0/S3
      RN0(2,2)=1.D0/S3
      RN0(3,2)=1.D0/S3
      RM0(1,2)=1.D0/S2
      RM0(2,2)=0.D0
      RM0(3,2)=1.D0/S2
!C A6
      RN0(1,3)=-1.D0/S3
      RN0(2,3)=1.D0/S3
      RN0(3,3)=1.D0/S3
      RM0(1,3)=1.D0/S2
      RM0(2,3)=1.D0/S2
      RM0(3,3)=0.D0
!C B2
      RN0(1,4)=1.D0/S3
      RN0(2,4)=1.D0/S3
      RN0(3,4)=1.D0/S3
      RM0(1,4)=0.D0
      RM0(2,4)=-1.D0/S2
      RM0(3,4)=1.D0/S2
!C B4
      RN0(1,5)=1.D0/S3
      RN0(2,5)=1.D0/S3
      RN0(3,5)=1.D0/S3
      RM0(1,5)=-1.D0/S2
      RM0(2,5)=0.D0
      RM0(3,5)=1.D0/S2
!C B5
      RN0(1,6)=1.D0/S3
      RN0(2,6)=1.D0/S3
      RN0(3,6)=1.D0/S3
      RM0(1,6)=-1.D0/S2
      RM0(2,6)=1.D0/S2
      RM0(3,6)=0.D0
!C C1
      RN0(1,7)=-1.D0/S3
      RN0(2,7)=-1.D0/S3
      RN0(3,7)=1.D0/S3
      RM0(1,7)=0.D0
      RM0(2,7)=1.D0/S2
      RM0(3,7)=1.D0/S2
!C C3
      RN0(1,8)=-1.D0/S3
      RN0(2,8)=-1.D0/S3
      RN0(3,8)=1.D0/S3
      RM0(1,8)=1.D0/S2
      RM0(2,8)=0.D0
      RM0(3,8)=1.D0/S2
!C C5
      RN0(1,9)=-1.D0/S3
      RN0(2,9)=-1.D0/S3
      RN0(3,9)=1.D0/S3
      RM0(1,9)=-1.D0/S2
      RM0(2,9)=1.D0/S2
      RM0(3,9)=0.D0
!C D1
      RN0(1,10)=1.D0/S3
      RN0(2,10)=-1.D0/S3
      RN0(3,10)=1.D0/S3
      RM0(1,10)=0.D0
      RM0(2,10)=1.D0/S2
      RM0(3,10)=1.D0/S2
!C D4
      RN0(1,11)=1.D0/S3
      RN0(2,11)=-1.D0/S3
      RN0(3,11)=1.D0/S3
      RM0(1,11)=-1.D0/S2
      RM0(2,11)=0.D0
      RM0(3,11)=1.D0/S2
!C D6
      RN0(1,12)=1.D0/S3
      RN0(2,12)=-1.D0/S3
      RN0(3,12)=1.D0/S3
      RM0(1,12)=1.D0/S2
      RM0(2,12)=1.D0/S2
      RM0(3,12)=0.D0
      ENDIF
!C A1
      RN0(1,1)=1.D0/S3
      RN0(2,1)=1.D0/S3
      RN0(3,1)=1.D0/S3
      RM0(1,1)=0.D0
      RM0(2,1)=1.D0/S2
      RM0(3,1)=-1.D0/S2
!C A2
      RN0(1,2)=1.D0/S3
      RN0(2,2)=1.D0/S3
      RN0(3,2)=1.D0/S3
      RM0(1,2)=-1.D0/S2
      RM0(2,2)=0.D0
      RM0(3,2)=1.D0/S2
!C A3
      RN0(1,3)=1.D0/S3
      RN0(2,3)=1.D0/S3
      RN0(3,3)=1.D0/S3
      RM0(1,3)=1.D0/S2
      RM0(2,3)=-1.D0/S2
      RM0(3,3)=0.D0
!C B1
      RN0(1,4)=-1.D0/S3
      RN0(2,4)=-1.D0/S3
      RN0(3,4)=1.D0/S3
      RM0(1,4)=0.D0
      RM0(2,4)=-1.D0/S2
      RM0(3,4)=-1.D0/S2
!C B2
      RN0(1,5)=-1.D0/S3
      RN0(2,5)=-1.D0/S3
      RN0(3,5)=1.D0/S3
      RM0(1,5)=1.D0/S2
      RM0(2,5)=0.D0
      RM0(3,5)=1.D0/S2
!C B3
      RN0(1,6)=-1.D0/S3
      RN0(2,6)=-1.D0/S3
      RN0(3,6)=1.D0/S3
      RM0(1,6)=-1.D0/S2
      RM0(2,6)=1.D0/S2
      RM0(3,6)=0.D0
!C C1
      RN0(1,7)=-1.D0/S3
      RN0(2,7)=1.D0/S3
      RN0(3,7)=1.D0/S3
      RM0(1,7)=0.D0
      RM0(2,7)=1.D0/S2
      RM0(3,7)=-1.D0/S2
!C C2
      RN0(1,8)=-1.D0/S3
      RN0(2,8)=1.D0/S3
      RN0(3,8)=1.D0/S3
      RM0(1,8)=1.D0/S2
      RM0(2,8)=0.D0
      RM0(3,8)=1.D0/S2
!C C3
      RN0(1,9)=-1.D0/S3
      RN0(2,9)=1.D0/S3
      RN0(3,9)=1.D0/S3
      RM0(1,9)=-1.D0/S2
      RM0(2,9)=-1.D0/S2
      RM0(3,9)=0.D0
!C D1
      RN0(1,10)=1.D0/S3
      RN0(2,10)=-1.D0/S3
      RN0(3,10)=1.D0/S3
      RM0(1,10)=0.D0
      RM0(2,10)=-1.D0/S2
      RM0(3,10)=-1.D0/S2
!C D2
      RN0(1,11)=1.D0/S3
      RN0(2,11)=-1.D0/S3
      RN0(3,11)=1.D0/S3
      RM0(1,11)=-1.D0/S2
      RM0(2,11)=0.D0
      RM0(3,11)=1.D0/S2
!C D3
      RN0(1,12)=1.D0/S3
      RN0(2,12)=-1.D0/S3
      RN0(3,12)=1.D0/S3
      RM0(1,12)=1.D0/S2
      RM0(2,12)=1.D0/S2
      RM0(3,12)=0.D0
!C
      DO IS=1,12 
!C    Compute Schmid tensor SCHM          
          DO I=1,3
              DO J=1,3
                  SCHM(I,J,IS)=RM0(I,IS)*RN0(J,IS)
              ENDDO
          ENDDO
!C    Compute symmetric and anti-symmetric part of Schmid tensor
          CALL getsym(SCHM(:,:,IS), 3, schmid_sym(:,:,IS))
          CALL getskw(SCHM(:,:,IS), 3, schmid_skw(:,:,IS))
!C    Converting to vectors   
          CALL MAT2NN(schmid_sym(:,:,IS), tmp)
          schmid_sym_vec(:,IS) = tmp(2:)
          schmid_skw_vec(:,IS) = [schmid_skw(2,3,IS),
     +                            schmid_skw(1,3,IS),
     +                            schmid_skw(1,2,IS)]
      ENDDO
      
!C
      RETURN
      END SUBROUTINE SCHMID       
!**********************************************************************!
!     Subroutine for HypoElastic-VP Crystal Plasticity                 ! 
! NN(Natural Notation), CCCS (coorotational crystal coordinate system  !
!(IO) Svec(NN,CCCS) Stress(n)                                          !
!(I)  Dvec(NN,CCCS) Total rate of deformation                          !
!(I)  Wn  (CCCS)    Spin Tensor                                        !
!(IO) RC - Rotation tensor                                             !
!           - From old coorotational crystal to global basis RC=QC^T   !     
!(IO) TauC  - Critical resolved shear stress                           !
!(IO) GammaA - Total shear strain of one slip system                   !
!(I)  Cdiag - Elastic tensor                                           !
!(O)  GAMMADOT - Shear strain rates on slip systems                    !
!(O)  Sdot- stress rate                                                !
!(I)  DT - Time incremental                                            !
!(I)  CMOD - Material Parameter                                        !
!(O)  WE - Rate of Non-Plastic Spin                                    !      
!(O)  PLSWORK - Plastic work                                           !
!(IO) EPSP                                                             !     
!(IO) FLAG - Flag indiciting running results                           !
!           I: 10 - First step of ME integration; 20 - Second step     !
!           0:  0 - Normal; -1*FLAG: Two large time second             !
!**************************************************************************
      SUBROUTINE SOLVER(Svec, Dvec,Wn, RC, 
     & TauC, GammaA, Cdiag, GAMMADOT, Sdot, DT, CMOD,
     & WE, PLSWORK, EPSP, FLAG, SKWSCHM, SYMSCHM, mode)
	
      IMPLICIT NONE
c     Input/Output variable definition
      REAL*8 Dvec(6), Wn(3,3), Svec(6), RC(3,3), 
     + TauC(12), GammaA(12), Cdiag(6),GAMMADOT(12),
     + Sdot(6), DT, CMOD(*), WE(3,3), PLSWORK, EPSP,
     +  SYMSCHM(5,12), SKWSCHM(3,12)
      INTEGER FLAG       
c     INTERNAL      
      REAL*8 MAXGMMMAPER, Tau(12), DP(6), DE(6),
     +  GAMMAD0, RATEMI, FF, tm(3), EPSPDOT, inner, vdot  
      REAL(8), dimension(3,3) ::  WP, TMP, DR 
      INTEGER NST,I,J,IS, HARMOD,SUBSTEPNR,GAMMAMODE, mode
c
c     --------------------------- Main program ------------------------
      HARMOD = CMOD(1)
	GAMMAD0 = CMOD(3)
      RATEMI = 1.D0/CMOD(4)
      MAXGMMMAPER = CMOD(11)     
      NST =         CMOD(12)
      SUBSTEPNR =   CMOD(13)
      GAMMAMODE =   CMOD(14)
      PLSWORK = 0.D0
      EPSPDOT = 0.D0
c 
c     --------------------- Single Crystal Program --------------------
c     Resolved shear stress  
      DO IS=1,12
          Tau(IS) = vdot(Svec(2:), SYMSCHM(:,IS), 5)
      ENDDO
c
c     Shear rate   ((GAMMAMODE 11))    
      IF (GAMMAMODE==11) THEN
          DO IS=1,12
              GAMMADOT(IS)=GAMMAD0*(Tau(IS)/TauC(IS))*
     +        (DABS(Tau(IS)/TauC(IS)))**(RATEMI-1.D0)	 
              IF(DABS(GAMMADOT(IS)*DT)>MAXGMMMAPER) THEN	     
                  IF(FLAG==10) THEN
	            WRITE(*,*) 'Too large Gamma dot at first ME step, 
     +                  use smaller time steps!', GAMMADOT(IS),DT,NST
                      FLAG = -1*FLAG
                      GOTO 110
                  ENDIF
                  IF(FLAG==20) THEN
                      FLAG = -1*FLAG 
                      GOTO 110
                  ENDIF
              ENDIF
          ENDDO 	   
c	   
      ELSEIF (GAMMAMODE==22) THEN       
c         Glide occurs only if the resolved shear stress is higher or 
c         equal to the slip resistance
          DO IS=1,12
              IF (DABS(Tau(IS)).GE.DABS(TauC(IS))) THEN
                  GAMMADOT(IS)=GAMMAD0*DSIGN(1.D0,Tau(IS))* 
     +            ((DABS(Tau(IS)/TauC(IS)))**RATEMI - 1.0D0)        
              ELSE
                  GAMMADOT(IS)=0.D0
              ENDIF
c		
              IF(DABS(GAMMADOT(IS)*DT)>MAXGMMMAPER) THEN	     
	            IF(FLAG==10) THEN
                      FLAG = -1*FLAG
                      GOTO 110
                  ENDIF
                  IF(FLAG==20) THEN
                      FLAG = -1*FLAG
                      GOTO 110
                  ENDIF
              ENDIF
          ENDDO	 
c	   
      ELSE
          PAUSE '**** WRONG FLOW RULE!'	  
      ENDIF
c      
c     Record the plastic work
      DO IS=1,12
          PLSWORK = PLSWORK+DABS(Tau(IS)*GAMMADOT(IS))*DT  
      ENDDO
c
c     -----------------------------------------------------------------
c     Calculating DP and WP in the coorotational crystal basis
      DP=0.D0
      WP=0.D0
      tm= 0.0d0
c  
      DO IS=1,12
          DP(2:)=DP(2:)+GAMMADOT(IS)*SYMSCHM(:,IS)
          tm = tm + SKWSCHM(:,IS)*GAMMADOT(IS)
      ENDDO
c
      WP=0.d0
      WP(1,2)= tm(3)
      WP(1,3)= tm(2)
      WP(2,3)= tm(1)
      WP(2,1)= -1.d0* WP(1,2)
      WP(3,1)= -1.d0* WP(1,3)
      WP(3,2)= -1.d0* WP(2,3)
c
c     -----------------------------------------------------------------
c     Calculating DE and WE in the coorotational crystal basis      
      DE=Dvec-DP
      WE=Wn-WP
c
c     -----------------------------------------------------------------
      Sdot= Cdiag*DE
c     Update Stress
      IF (mode==1) THEN
c         hydrostatic pressure update
          Svec(1) = Svec(1) + Cdiag(1)*DE(1)*DT
c         deviatoric Stress update          
          Svec(2:) = Svec(2:) + Sdot(2:)*DT
      ENDIF
c
c     -----------------------------------------------------------------
c     Von-Mises plastic strain      
      EPSPDOT = DSQRT(vdot(DP(2:),DP(2:),5)*2.D0/3.D0)
      EPSP = EPSP + EPSPDOT*DT     
c
c     For first step, make sure it's pure elastic---
      IF((NST==1).AND.(SUBSTEPNR<1)) THEN
          FF=0.0D0
          DO IS=1,12
              FF=FF+DABS(GAMMADOT(IS))*DABS(GAMMADOT(IS))
          ENDDO
c
          IF(FF>1.0D-12) THEN
 	    !WRITE(*,*) '!------------------   INFO        ------------------!'
 	    !WRITE(*,*) 'Too large time step for the first step  step!'
 	    !WRITE(*,*) 'Substepping will start at the first step!'
 	    !WRITE(*,*) '!---------------------------------------------------!'
 	    !WRITE(*,*) '                  '
              FLAG = -1*FLAG
              GOTO 110
          ENDIF
      ENDIF	 
c
c	------------------------ Work Hardening -------------------------
      IF(HARMOD == 1) THEN
c         PAN type
          CALL GSRDOTPANHYPO(CMOD,GammaA,GAMMADOT,TauC,DT)    
      ELSEIF(HARMOD == 2) THEN
c         Kalidinal Type 
          CALL KALIDHARDHYPO(CMOD,GAMMADOT,TauC,DT) 
      ELSEIF((HARMOD == 3) .OR. (HARMOD == 4)) THEN
c         Voce or Generalised voce 
          CALL VOCEHARDHYPO(CMOD,GammaA,GAMMADOT,TauC,DT)	   
      ELSEIF(HARMOD == 5) THEN
c	    Johson-cook model
          CALL JohsonCookModel(CMOD,EPSP,EPSPDOT,TauC)	   
      ENDIF
c
c     Update total shear of each slip systems
      IF (mode==1) THEN
          DO IS=1,12
              GammaA(IS) = GammaA(IS) + DABS(GAMMADOT(IS)*DT)
          ENDDO
      ENDIF
c	 
110	RETURN
      END SUBROUTINE SOLVER
!C**********************************************************************
!C                        SUBROUTINE KALIDHARD                         *
!C**********************************************************************
!C    KALIDHARD Hardening 	             					         *
!C**********************************************************************
      SUBROUTINE KALIDHARDHYPO(CMOD,GAMMAD,GSR,DT)
      IMPLICIT NONE
      
      REAL*8 CMOD(*),GAMMAD(12),GSR(12),GSRDOT(12),HSS(12,12),GG,DT
      INTEGER IS,JS

      !Get the hardening matrix
      CALL HARDKALHYPO(CMOD,GSR,HSS)
      DO IS=1,12
        GG=0.D0
        DO JS=1,12
          GG=GG+HSS(IS,JS)*DABS(GAMMAD(JS))
        ENDDO
        GSRDOT(IS)=GG
      ENDDO

!Work hardening
      DO IS=1,12
	   GSR(IS) = GSR(IS)+GSRDOT(IS)*DT
      ENDDO
	
      RETURN
      END SUBROUTINE KALIDHARDHYPO
!C**********************************************************************
!C                         SUBROUTINE HARDKAL                          *
!C**********************************************************************
!C Computes the hardening matrix                                       *
!C Kalidindi proposes another form for high strain rates and/or high   *
!C  homologous temperatures                                            *
!C**********************************************************************
      SUBROUTINE HARDKALHYPO(CMOD,GSR,HSS)
!C
      IMPLICIT NONE
!C
      REAL*8 GSR(*),CMOD(*),HSS(12,12),HB,H0,TAUS,CQ,CA
      INTEGER JS,IS
	    
!C
      CQ=CMOD(6)
      H0=CMOD(7)
      CA=CMOD(8)
      TAUS=CMOD(9)
	  
!C Compute the components of the hardening matrix
!C  that is diagonal by blocks
      DO IS=1,12
        DO JS=1,12
          IF (GSR(JS).LE.TAUS) THEN
            HB=H0*(1.D0-GSR(JS)/TAUS)**CA
          ELSE
            HB=0.D0
          ENDIF
          IF (((IS.LE.3).AND.(JS.LE.3)).OR.((IS.GE.4).AND.(IS.LE.6).AND.
     &         (JS.GE.4).AND.(JS.LE.6)).OR.((IS.GE.7).AND.(IS.LE.9).AND.
     &         (JS.GE.7).AND.(JS.LE.9)).OR.((IS.GE.10).AND.(IS.LE.12).
     &         AND.(JS.GE.10).AND.(JS.LE.12))) THEN
            HSS(IS,JS)=HB
          ELSE
            HSS(IS,JS)=HB*CQ
          ENDIF
        ENDDO
      ENDDO
!C
      RETURN
      END SUBROUTINE HARDKALHYPO
!C**********************************************************************
!C                         SUBROUTINE GSRDOTPANHYPO                          *
!C**********************************************************************
!C  Pierce-Asaro-Needleman HYPO Hardening 	             					         *
!C**********************************************************************
      SUBROUTINE GSRDOTPANHYPO(CMOD,GAMMA,GAMMAD,GSR,DT)
      IMPLICIT NONE    

      REAL*8 CMOD(*),GAMMA(12),GAMMAD(12),GSR(12),GSRDOT(12),
     &  HSS(12,12),GG,DT
      INTEGER IS,JS

!C    Get the hardening matrix
      CALL GSRDOTPAN(CMOD,GAMMA,GAMMAD,GSRDOT)
	  
!C    Work hardening
      DO IS=1,12
	   GSR(IS) = GSR(IS)+GSRDOT(IS)*DT
      ENDDO
	
      RETURN
      END	SUBROUTINE GSRDOTPANHYPO  
!C**********************************************************************
!C                         SUBROUTINE GSRDOTPAN                        *
!C**********************************************************************
!C Computes the slip resistance evolution                              *
!C**********************************************************************
      SUBROUTINE GSRDOTPAN(CMOD,GAMMA,GAMMAD,STVD)
!C
      IMPLICIT NONE
!C
      REAL*8 GAMMA(12),GAMMAD(12),STVD(*),CMOD(*),HSS(12,12),GG
      INTEGER IS,JS
!C
!C Get the hardening matrix
      CALL HARDPAN(CMOD,GAMMA,HSS)
!C Compute the slip resistance rate
      DO IS=1,12
        GG=0.D0
        DO JS=1,12
          GG=GG+HSS(IS,JS)*DABS(GAMMAD(JS))
        ENDDO
        STVD(IS)=GG
      ENDDO
!C
      RETURN
      END SUBROUTINE GSRDOTPAN
!C**********************************************************************
!C                         SUBROUTINE HARDPAN                          *
!C**********************************************************************
!C Computes the hardening matrix                                       *
!C Model for strong alloy crystals                                     *
!C Peirce, Asaro, Needleman propose another form for pure single phase *
!C  crystal with no saturation                                         *
!C**********************************************************************
      SUBROUTINE HARDPAN(CMOD,GAMMA,HSS)
!C
      IMPLICIT NONE
!C
      REAL*8 GAMMA(12),CMOD(*),HSS(12,12),GAMMAT,HSH,H0,TAUS,TAU0,CQ
      INTEGER IS,JS
!C
      TAU0=CMOD(5)
      CQ=CMOD(6)
      H0=CMOD(7)
      TAUS=CMOD(8)      
!C Compute the sum of the absolute slips
      GAMMAT=0.D0
      DO IS=1,12
        GAMMAT=GAMMAT+DABS(GAMMA(IS))
      ENDDO
!C Compute the self-hardening coefficient
      HSH=H0/(DCOSH(H0*GAMMAT/(TAUS-TAU0)))**2.D0
!C Compute the components of the hardening matrix
!C  that is diagonal by blocks
      DO IS=1,12
        DO JS=1,12
          HSS(IS,JS)=HSH*CQ
        ENDDO
      ENDDO
      DO IS=1,3
        DO JS=1,3
          HSS(IS,JS)=HSH
          HSS(IS+3,JS+3)=HSH
          HSS(IS+6,JS+6)=HSH
          HSS(IS+9,JS+9)=HSH
        ENDDO
      ENDDO
!C
      RETURN
      END SUBROUTINE HARDPAN
!C**********************************************************************
!C                         SUBROUTINE VOCEHARDHYPO                          *
!C**********************************************************************
!C  Generalised Voce Hardening 	             					         *
!C**********************************************************************
      SUBROUTINE VOCEHARDHYPO(CMOD,GAMMA,GAMMAD,GSR,DT)
      IMPLICIT NONE
      
      REAL*8 CMOD(*),GAMMA(12),GAMMAD(12),GSR(12),GSRDOT(12),
     &  HSS(12,12),GG,DT
      INTEGER IS,JS

!C   Get the hardening matrix
      CALL GSRDOTVOCE(CMOD,GAMMA,GAMMAD,GSRDOT)

!C   Work hardening
      DO IS=1,12
	   GSR(IS) = GSR(IS)+GSRDOT(IS)*DT
      ENDDO
	
      RETURN
      END SUBROUTINE VOCEHARDHYPO
!C**********************************************************************
!C                         SUBROUTINE GSRDOTVOCE                       *
!C**********************************************************************
!C Computes the slip resistance evolution                              *
!C**********************************************************************
      SUBROUTINE GSRDOTVOCE(CMOD,GAMMA,GAMMAD,STVD)
!C
      IMPLICIT NONE
!C
      REAL*8 GAMMA(12),GAMMAD(12),STVD(*),CMOD(*),HSS(12,12),GG
      INTEGER IS,JS
!C
!C Get the hardening matrix
      CALL HARDVOCE(CMOD,GAMMA,HSS)
!C Compute the slip resistance rate
      DO IS=1,12
        GG=0.D0
        DO JS=1,12
          GG=GG+HSS(IS,JS)*DABS(GAMMAD(JS))
        ENDDO
        STVD(IS)=GG
      ENDDO
!C
      RETURN
      END SUBROUTINE GSRDOTVOCE
!C**********************************************************************
!C                         SUBROUTINE HARDVOCE                         *
!C**********************************************************************
!C Computes the hardening matrix (for Voce or generalised Voce)        *
!C The latent hardening is supposed to be the same for all slip systems*
!C**********************************************************************
      SUBROUTINE HARDVOCE(CMOD,GAMMA,HSS)
!C
      IMPLICIT NONE
!C
      REAL*8 GAMMA(12),CMOD(*),HSS(12,12),GAMMAT,HSH,THETA0,TAU1,
     &       THETA1,CQ,THETA(2+CMOD(2)),TAU(2+CMOD(2))
      INTEGER IS,JS,I
!C
      CQ=CMOD(6)
      IF (CMOD(1).EQ.4.D0) THEN
!C    Generalised Voce
        THETA0=CMOD(7)
        TAU1=CMOD(8)
        THETA1=CMOD(9)
      ELSE ! Voce
        DO I=1,CMOD(2)
          THETA(I)=CMOD(7+2*(I-1))
          TAU(I)=CMOD(8+2*(I-1))
        ENDDO
      ENDIF
!C Compute the sum of the absolute slips
      GAMMAT=0.D0
      DO IS=1,12
        GAMMAT=GAMMAT+DABS(GAMMA(IS))
      ENDDO
!C Compute the hardening coefficient
      HSH=0.D0
      IF (CMOD(1).EQ.4.D0) THEN
!C Generalised Voce
        HSH=THETA1+(THETA0-THETA1+THETA0*THETA1/TAU1*GAMMAT)*
     &      DEXP(-THETA0*GAMMAT/TAU1)
      ELSE
!C Voce
        DO I=1,CMOD(2)
          HSH=HSH+THETA(I)*DEXP(-THETA(I)*GAMMAT/TAU(I))
        ENDDO
      ENDIF
	  
!C Compute the components of the hardening matrix
      DO IS=1,12
        DO JS=1,12
            HSS(IS,JS)=HSH*CQ
        ENDDO
      ENDDO
	  
      DO IS=1,3
        DO JS=1,3
          HSS(IS,JS)=HSH
          HSS(IS+3,JS+3)=HSH
          HSS(IS+6,JS+6)=HSH
          HSS(IS+9,JS+9)=HSH
        ENDDO
      ENDDO
!C
      RETURN
      END SUBROUTINE HARDVOCE
c**********************************************************************
c                      SUBROUTINE JohsonCookModel                     *
c**********************************************************************
c  Johson-cook Hardening              					         *
c**********************************************************************
      SUBROUTINE JohsonCookModel(CMOD,EPSP,EPSPDOT,GSR)
      IMPLICIT NONE
      
      REAL*8 CMOD(*),EPSPDOT, EPSP,GSR(12), AA, BB, CC, NN, EPS_0, GG
      INTEGER IS,JS
c    work hardening parameters
      AA = CMOD(5)
	BB = CMOD(6)
      CC = CMOD(7)
      NN = CMOD(8)	
      EPS_0 = CMOD(9)
	GG = 0.D0   
c     Work hardening
c     WRITE(523,*) 'AA, BB, NN, CC,EPS_0', AA, BB, NN, CC ,EPS_0
      GG = (AA + BB*(EPSP**NN)) !*(1.D0+CC*DLOG((EPSPDOT+1.E-15)/EPS_0))
c     WRITE(523,*) 'GSR', GG

c     Work hardening
      DO IS=1,12
       GSR(IS) = GG
      ENDDO
	
      RETURN
      END SUBROUTINE JohsonCookModel
c**********************************************************************
c                         SUBROUTINE EULER                            *
c**********************************************************************
c Computes the Euler angles associated with the orientation matrix    *
c in terms of the three Euler angles: phi1 (rotation about Z1), PHI   *
c (rotation about X2) and phi2 (rotation about Z3) - Bunge notation   *
c**********************************************************************
      SUBROUTINE EULERU44(Q,ANG)
c
      IMPLICIT NONE
c
      REAL*8 Q(3,3),ANG(3),ANG1,ANG2,ANG3,RPI,STH
c
      RPI=4.D0*DATAN(1.D0)
      IF(DABS(Q(3,3))>1.D0) THEN
       CALL NORMALMAT(Q)
      ENDIF
	  
      IF(DABS(DABS(Q(3,3))-1.D0)<1.D-6) THEN
 	    ANG1=DATAN2(Q(1,2),Q(1,1))
        ANG2=0.D0
        ANG3=0.D0       
   	  ELSE 
        ANG2=DACOS(Q(3,3))
        STH=DSIN(ANG2)
        ANG1=DATAN2(Q(3,1)/STH,-Q(3,2)/STH)
        ANG3=DATAN2(Q(1,3)/STH,Q(2,3)/STH)
      ENDIF
      ANG(1)=ANG1*180.D0/RPI
      ANG(2)=ANG2*180.D0/RPI
      ANG(3)=ANG3*180.D0/RPI
c
      RETURN
      END SUBROUTINE EULERU44
c**********************************************************************
c                         SUBROUTINE ORIENTSL                         *
c**********************************************************************
c Computes the orientation matrix from sample basis to lattice basis  *
c in terms of the three Euler angles: phi1 (rotation about Z1), PHI   *
c (rotation about X2) and phi2 (rotation about Z3) - Bunge notation   *
c**********************************************************************
      SUBROUTINE ORIENTSL(ANG,Q0)
c
      IMPLICIT NONE
c
      REAL*8 ANG(3),Q0(3,3),ANG1,ANG2,ANG3,RPI
c
      RPI=4.D0*DATAN(1.D0)
      ANG1=RPI*ANG(1)/180.D0
      ANG2=RPI*ANG(2)/180.D0
      ANG3=RPI*ANG(3)/180.D0
      Q0(1,1)=DCOS(ANG1)*DCOS(ANG3)-DSIN(ANG1)*DCOS(ANG2)*DSIN(ANG3)
      Q0(1,2)=DSIN(ANG1)*DCOS(ANG3)+DCOS(ANG1)*DCOS(ANG2)*DSIN(ANG3)
      Q0(1,3)=DSIN(ANG2)*DSIN(ANG3)
      Q0(2,1)=-DCOS(ANG1)*DSIN(ANG3)-DSIN(ANG1)*DCOS(ANG2)*DCOS(ANG3)
      Q0(2,2)=-DSIN(ANG1)*DSIN(ANG3)+DCOS(ANG1)*DCOS(ANG2)*DCOS(ANG3)
      Q0(2,3)=DSIN(ANG2)*DCOS(ANG3)
      Q0(3,1)=DSIN(ANG1)*DSIN(ANG2)
      Q0(3,2)=-DCOS(ANG1)*DSIN(ANG2)
      Q0(3,3)=DCOS(ANG2)
c
      RETURN
      END SUBROUTINE ORIENTSL
!C**********************************************************************
!C                         SUBROUTINE MINV3                            *
!C**********************************************************************
!C Computes the inverse of a (3x3) matrix AINV = A^-1                  *
!C**********************************************************************
      SUBROUTINE MINV3(A)
!C
      IMPLICIT NONE
!C
      REAL*8 A(3,3),ADJ(3,3),DET
      INTEGER I,J
!C
!C Compute the determinant of A
      CALL DETERM(A,DET)
!C Compute the adjoint matrix of A
      ADJ(1,1)=A(2,2)*A(3,3)-A(3,2)*A(2,3)
      ADJ(1,2)=-(A(2,1)*A(3,3)-A(3,1)*A(2,3))
      ADJ(1,3)=A(2,1)*A(3,2)-A(3,1)*A(2,2)
      ADJ(2,1)=-(A(1,2)*A(3,3)-A(3,2)*A(1,3))
      ADJ(2,2)=A(1,1)*A(3,3)-A(3,1)*A(1,3)
      ADJ(2,3)=-(A(1,1)*A(3,2)-A(3,1)*A(1,2))
      ADJ(3,1)=A(1,2)*A(2,3)-A(2,2)*A(1,3)
      ADJ(3,2)=-(A(1,1)*A(2,3)-A(2,1)*A(1,3))
      ADJ(3,3)=A(1,1)*A(2,2)-A(2,1)*A(1,2)
!C Compute the transpose of the adjoint matrix of A = inverse of A
      DO I=1,3
        DO J=1,3
          A(I,J)=ADJ(J,I)/DET
        ENDDO
      ENDDO
!C
      RETURN
      END SUBROUTINE MINV3
!C**********************************************************************
!C                         SUBROUTINE MMULT                            *
!C**********************************************************************
!C Computes the product of two (NxN) matrices AB = A.B                 *
!C**********************************************************************
      SUBROUTINE MMULT(A,B,N,AB)
C
      IMPLICIT NONE
C
      REAL*8 A(N,N),B(N,N),AB(N,N),P
      INTEGER I,J,K,N
C
      DO I=1,N
         DO J=1,N
            P=0.D0
            DO K=1,N
               P=P+A(I,K)*B(K,J)
            ENDDO
            AB(I,J)=P
         ENDDO
      ENDDO
C
      RETURN
      END SUBROUTINE MMULT
!C**********************************************************************
!C                         SUBROUTINE MVMULT                            *
!C**********************************************************************
!C Computes the product of two matrices, A(NxN), B(Nx1) AB = A.B                 *
!C**********************************************************************
      SUBROUTINE MVMULT(A,V,N,Z)
C
      IMPLICIT NONE
C
      REAL*8 A(N,N),Z(N),V(N),P
      INTEGER I,K,N
C
      DO I=1,N
         P=0.D0
         DO K=1,N
            P=P+A(I,K)*V(K)
         ENDDO
         Z(I)=P
      ENDDO
C
      RETURN
      END SUBROUTINE MVMULT
!C**********************************************************************
!C                         SUBROUTINE MTRANSP                          *
!C**********************************************************************
!C Computes the transpose of a (3x3) matrix AT = A^T                   *
!C**********************************************************************
      SUBROUTINE MTRANSP(A,N,AT)
C
      IMPLICIT NONE
C
      REAL*8 A(N,N),AT(N,N)
      INTEGER I,J,N
C
      DO I=1,N
         DO J=1,N
            AT(I,J)=A(J,I)
         ENDDO
      ENDDO
C
      RETURN
      END SUBROUTINE MTRANSP
!C**********************************************************************
!C                         SUBROUTINE MNORM                            *
!C**********************************************************************
!C Computes the norm of a (RxC) matrix using double contraction        *
!C Norm = (Tr(A.A^T))^1/2 = (A:A)^1/2                                  *
!C**********************************************************************
      SUBROUTINE MNORM(A,R,C,RNORM)
!C
      IMPLICIT NONE
!C
      INTEGER R,C
      REAL*8 A(R,C),SUM,RNORM
      INTEGER I,J
!C
      SUM=0.D0
      DO I=1,R
        DO J=1,C
          SUM=SUM+A(I,J)*A(I,J)
        ENDDO
      ENDDO
!C
      RNORM=DSQRT(SUM)
!C
      RETURN
      END SUBROUTINE MNORM
!C**********************************************************************
!C Normalise the input matrix				         	                 *
!C**********************************************************************
      SUBROUTINE NORMALMAT(MAT)
      IMPLICIT NONE
	
      REAL*8 MAT(3,3), DETFP
      INTEGER I,J

      CALL DETERM(MAT,DETFP)
      IF(DETFP>1.D0) THEN
      DO I=1,3
       DO J=1,3
          MAT(I,J)=MAT(I,J)/DETFP**(1.D0/3.0D0)
       ENDDO
      ENDDO
      ENDIF

      RETURN
      END SUBROUTINE NORMALMAT
!C**********************************************************************
!C                         SUBROUTINE DETERM                           *
!C**********************************************************************
!C Computes the determinant of a (3x3) matrix                          *
!C**********************************************************************
      SUBROUTINE DETERM(A,DET)
!C
      IMPLICIT NONE
!C
      REAL*8 A(3,3),DET
!C
      DET=A(1,1)*A(2,2)*A(3,3)+
     &    A(2,1)*A(3,2)*A(1,3)+
     &    A(3,1)*A(1,2)*A(2,3)-
     &    A(3,1)*A(2,2)*A(1,3)-
     &    A(1,1)*A(3,2)*A(2,3)-
     &    A(2,1)*A(1,2)*A(3,3)
!C
      IF (DABS(DET).LT.1.D-12) THEN
          WRITE(*,*)'Determinant is null!'
          WRITE(59,*)'Determinant is null!'
      ENDIF
!C
      RETURN
      END SUBROUTINE DETERM
c     =================================================================      
c     =================================================================
      SUBROUTINE TRANSFORM(A,P,N,B)
C     Rotates a matrix from one frame to another
C     B = P^T.A.P
      IMPLICIT NONE
C
      REAL*8 A(N,N),B(N,N),P(N,N),PT(N,N),MAT(N,N)
      INTEGER N
C     
      CALL MTRANSP(P,N,PT)
      CALL MMULT(A,P,N,MAT)
      CALL MMULT(PT,MAT,N,B)
C
      RETURN
      END SUBROUTINE TRANSFORM
c     =================================================================
c     ================================================================= 
      SUBROUTINE TRANSFORMB(A,P,N,B)
C     Rotates a matrix from one frame to another
C     B = P.A.P^T
      IMPLICIT NONE
C     
      REAL*8 A(N,N),B(N,N),P(N,N),PT(N,N),MAT(N,N)
      INTEGER N
C     
      CALL MTRANSP(P,N,PT)
      CALL MMULT(A,PT,N,MAT)
      CALL MMULT(P,MAT,N,B)
C
      RETURN
      END SUBROUTINE TRANSFORMB
c     =================================================================
c     ================================================================= 
      subroutine getsym(A, N, ASYM)
c
      implicit none
c
      real(8) :: A(N,N), ASYM(N,N)
	  integer :: i, j, N
c
      do i=1,N
	     do j=1,N
		    ASYM(i,j) = 0.5d0*(A(i,j)+A(j,i))
		 end do
      end do
c
      return
      end subroutine getsym
c     =================================================================
c     =================================================================
      subroutine getskw(A, N, ASKW)
c
      implicit none
c
      real(8) :: A(N,N), ASKW(N,N)
	  integer :: i, j, N
c
      do i=1,N
	     do j=1,N
		    ASKW(i,j) = 0.5d0*(A(i,j)-A(j,i))
		 end do
      end do
c
      return
      end subroutine getskw
c     =================================================================
c     ================================================================= 
      subroutine MAT2NN(A, v)
c
c     Transforms symmetric 3x3 matrix A, 
c     into a 6x1 vector v written in new notation       
c
      implicit none
c
      real(8) :: A(3,3), v(6)
c
	v(1) = 1.d0/sqrt(3.d0)*(A(1,1)+A(2,2)+A(3,3))
	v(2) = 1.d0/sqrt(6.d0)*(2.d0*A(3,3)-A(1,1)-A(2,2))
	v(3) = 1.d0/sqrt(2.d0)*(A(2,2)-A(1,1))
	v(4) = sqrt(2.d0)*A(2,3)
	v(5) = sqrt(2.d0)*A(1,3)
	v(6) = sqrt(2.d0)*A(1,2)
c
      return
      end subroutine MAT2NN
c     =================================================================
c     =================================================================
      subroutine NN2MAT(v, A)
c   
c     Transforms 6x1 vector v written in new notation
c     into a symmetric 3x3 matrix A
c
      implicit none
c
      real(8) :: v(6), A(3,3)
c   
      A(1,1) = 1.d0/sqrt(3.d0)*v(1) - 1.d0/sqrt(6.d0)*v(2)
     +       - 1.d0/sqrt(2.d0)*v(3)
      A(2,2) = 1.d0/sqrt(3.d0)*v(1) - 1.d0/sqrt(6.d0)*v(2)
     +       + 1.d0/sqrt(2.d0)*v(3)
      A(3,3) = 1.d0/sqrt(3.d0)*v(1) + sqrt(2.d0/3.d0)*v(2)
      A(2,3) = 1.d0/sqrt(2.d0)*v(4)
      A(1,3) = 1.d0/sqrt(2.d0)*v(5)
      A(1,2) = 1.d0/sqrt(2.d0)*v(6)
      A(3,2) = A(2,3)
      A(3,1) = A(1,3)
      A(2,1) = A(1,2)
c
      return
      end subroutine NN2MAT
c     =================================================================
c     =================================================================
      subroutine AV2MAT(Vec,Mat,sz)
c     
c     this subroutine converts the given vector of size sz=6 or 9 to a 
c     symmetric or non-symmetric matrix of size (3,3). component
c     ordering follows Abaqus/Explicit convention: 
c     Symmetric:     11-22-33-12-23-31
c     Non-Symmetric: 11-22-33-12-23-31-21-32-13
c           
      implicit none
c     
      integer :: sz
      real(8) :: Vec(sz),Mat(3,3)
      Mat=0.d0
      Mat(1,1)= Vec(1)
      Mat(2,2)= Vec(2)
      Mat(3,3)= Vec(3)
      Mat(1,2)= Vec(4)
      Mat(2,3)= Vec(5)
      Mat(3,1)= Vec(6)
      if (sz .EQ. 9.d0) then
          Mat(2,1)= Vec(7)
          Mat(3,2)= Vec(8)
          Mat(1,3)= Vec(9)
      else
          Mat(2,1)= Mat(1,2)
          Mat(3,2)= Mat(2,3)
          Mat(1,3)= Mat(3,1)
      endif
c      
      return      
      end subroutine AV2MAT
c     =================================================================
c     =================================================================
      subroutine MAT2AV(Mat,Vec,sz)
c     
c     this subroutine converts the given symmetric or non-symmetric
c     Matrix of size (3,3) to a vector of size sz=6 or 9. component
c     ordering follows Abaqus/Explicit convention: 
c     Symmetric:     11-22-33-12-23-31
c     Non-Symmetric: 11-22-33-12-23-31-21-32-13
c     
      implicit none
c     
      integer :: sz
      real(8) :: Vec(sz),Mat(3,3)
      Vec=0.0d0      
      Vec(1)= Mat(1,1)
      Vec(2)= Mat(2,2) 
      Vec(3)= Mat(3,3) 
      Vec(4)= Mat(1,2)
      Vec(5)= Mat(2,3)
      Vec(6)= Mat(3,1)
      if (sz .EQ. 9.d0) then
          Vec(7)= Mat(2,1)
          Vec(8)= Mat(3,2)
          Vec(9)= Mat(1,3)
      endif
c      
      return      
      end subroutine MAT2AV
c     =================================================================
c     ================================================================= 
      function vdot(x, y, n) result(v)
c          
      implicit none
      integer :: n, i
      real(8) :: x(n), y(n), v
c          
      v = 0.d0
      do i = 1, n
          v = v + x(i)*y(i)
      end do
c          
      return
      end function vdot
c     =================================================================    
c     =================================================================
      function inner(A, B) result(c)
c        
      implicit none
      integer :: i, j
      real(8)	:: A(3,3), B(3,3), c
c          
      c = 0.d0
      do i = 1,3
          do j = 1,3
              c = c + A(i,j)*B(i,j)
          end do
      end do
c          
      return
      end function inner
c     =================================================================
c     =================================================================      
      subroutine outer(x, y, n, A)
c
      implicit none
c
      integer :: i, j, n
      real(8)	:: x(n), y(n), A(n,n)
c
      A = 0.d0
      do i=1, n
          do j=1, n
              A(i,j) = x(i)*y(j)
          end do
      end do
c
      return
      end subroutine outer
c     =================================================================
c     =================================================================